import express from 'express';
import cors from 'cors';

const app = express();
app.use(cors());
app.get('/', (req, res) => {
  
  const name=req.query.fullname||'';
  const nameArr=name.replace(/\t/g,'').replace(/  +/g, ' ').trim().split(' ');
  let newName='';
  let lastname='';
  let firstname=''
  let middlename='';
  let rx=new RegExp('[\/0-9_\-]+');
  
 
  if(name&&!rx.test(name)&&nameArr.length>0&&nameArr.length<=3){
    lastname=capitalizeName(nameArr[nameArr.length-1]);
    if(nameArr.length>1){
    firstname=' '+capitalizeName(nameArr[0].substr(0,1)+'.');
    if(nameArr.length>2){
    middlename=' '+capitalizeName(nameArr[nameArr.length-2].substr(0,1)+'.');
    }
    }
    newName =lastname+firstname+middlename;
  }
  else{
    newName='Invalid fullname'
  }
  console.log(name);
  res.send(newName);
});
function  capitalizeName(name){
  return name.substr(0,1).toUpperCase()+name.substr(1).toLowerCase();
}
app.listen(3000, () => {
  console.log('Your app listening on port 3000!');
});
